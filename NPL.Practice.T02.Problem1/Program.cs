using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string content = "One of the world's biggest festivals hit the streets of London";
            int maxLength = 50;
            var result = GetArticleSummary(content, maxLength);


            Console.WriteLine(result);
            Console.ReadKey();
        }

        public static string GetArticleSummary(string content, int maxLength)
        {
            //use split to add a word to string array
            var splitContent = content.Split(' ');
            int count = 0;
            string result = "";

            foreach (var item in splitContent)
            {
                if (count + item.Length <= maxLength)
                {
                    result += item + " ";
                    count = result.Length;
                }
                else
                {
                    break;
                }
            }
            return result.TrimEnd() + "...";
        }
    }

}
