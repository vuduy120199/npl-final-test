using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int[][] inputArr = new int[][]
            {
                new int[] { 1, 2, 5 },
                new int[] { 2,5,-4 },
                new int[] { 5,-4,3 },
            };
            Console.WriteLine(FindMaxSubArray(inputArr, 3));
            Console.ReadKey();
        }

        public static int FindMaxSubArray(int[][] inputArray, int Sublength)
        {
            List<int> sumArr = new List<int>();
            int sum = 0;
            for (int i = 0; i < inputArray.Length; i++)
            {
                for (int j = 0; j < Sublength; j++)
                {
                    sum += inputArray[i][j];
                }
                sumArr.Add(sum);
                sum = 0;
            }

            return sumArr.Max();
        }
    }

}
