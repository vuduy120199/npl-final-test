using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            Console.WriteLine("nhap id");
            student.Id = Int32.Parse(Console.ReadLine());
            Console.WriteLine("nhap ten");
            student.Name = Console.ReadLine();
            Console.WriteLine("nhap sqlmark");
            student.SqlMark = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("nhap csharpmark");
            student.CsharpMark = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("nhap dsamark");
            student.DsaMark = Convert.ToDecimal(Console.ReadLine());
            student.Graduate();
            Console.WriteLine(student.GetCertificate());
            Console.ReadKey();
        }
    }
}
