using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem03
{
    public class Student : IGraduate
    {
        public int Id;
        public string Name;
        public DateTime StartDate;
        public decimal SqlMark;
        public decimal CsharpMark;
        public decimal DsaMark;
        public decimal GPA;
        public GraduateLevel graduateLevel;
        public enum GraduateLevel
        {
            Excellent,
            VeryGood,
            Good,
            Average,
            Failed
        }

        public void Graduate()
        {
            GPA = (SqlMark + CsharpMark + DsaMark) / 3;
            if (GPA >= 9)
            {
                graduateLevel = GraduateLevel.Excellent;
            }
            else if(GPA >= 8 && GPA < 9)
            {
                graduateLevel = GraduateLevel.VeryGood;
            }
            else if (GPA >= 7 && GPA < 8)
            {
                graduateLevel = GraduateLevel.Good;
            }
            else if (GPA >= 5 && GPA < 7)
            {
                graduateLevel = GraduateLevel.Average;
            }
            else if (GPA < 5)
            {
                graduateLevel = GraduateLevel.Failed;
            }
        }

        public string GetCertificate()
        {
            var studentInfo = $"Name: {Name}, SqlMark: {SqlMark}, CsharpMark: {CsharpMark}, DsaMark: {DsaMark}, GPA: {GPA}, GraduateLevel: {graduateLevel}";
            return studentInfo;
        }


    }
}
